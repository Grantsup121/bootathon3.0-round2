var vx1 = document.getElementById("vx1");
var vy1 = document.getElementById("vy1");
var vx2 = document.getElementById("vx2");
var vy2 = document.getElementById("vy2");
var vx3 = document.getElementById("vx3");
var vy3 = document.getElementById("vy3");
var ar = document.getElementById("ar");
function triangle() {
    if (Number(vx1) && Number(vy1) && Number(vx2) && Number(vy2) && Number(vx3) && Number(vy3) || vx1.value == '0' || vy1.value == '0' || vx2.value == '0' || vy2.value == '0' || vx3.value == '0' || vy3.value == '0') {
        var d1 = Math.sqrt(Math.pow(parseFloat(vx2.value) - parseFloat(vx1.value), 2) - Math.pow(parseFloat(vy2.value) - parseFloat(vy1.value), 2));
        var d2 = Math.sqrt(Math.pow(parseFloat(vx1.value) - parseFloat(vx3.value), 2) - Math.pow(parseFloat(vy1.value) - parseFloat(vy1.value), 2));
        var d3 = Math.sqrt(Math.pow(parseFloat(vx3.value) - parseFloat(vx2.value), 2) - Math.pow(parseFloat(vy3.value) - parseFloat(vy2.value), 2));
        var s = (d1 + d2 + d3) / 2;
        var m = s * (s - d1) * (s - d2) * (s - d3);
        if (m < 0) {
            ar.value = "triangle not possible";
        }
        else {
            var are = Math.sqrt(m);
            ar.value = are.toString();
        }
    }
    else {
        alert("Invalid input");
    }
}
//# sourceMappingURL=areatriangle.js.map