var s1 = document.getElementById("s1");
var s2 = document.getElementById("s2");
var s3 = document.getElementById("s3");
var c = document.getElementById("ans");
var r = document.getElementById("right");
//function for checking triangle type
function typ() {
    if (Number(s2) || Number(s1) || Number(s3) || s1.value == '0' || s2.value == '0' || s3.value == '0') {
        //condition for equilatral triangle
        if (parseFloat(s1.value) == parseFloat(s2.value) && parseFloat(s3.value) == parseFloat(s2.value)) {
            c.value = "Equilateral Triangle";
        } //condition for isosceles triangle
        else if (parseFloat(s1.value) == parseFloat(s2.value)) {
            c.value = "Isosceles Triangle";
        }
        else if (parseFloat(s2.value) == parseFloat(s3.value)) {
            c.value = "Isosceles Triangle";
        }
        else if (parseFloat(s1.value) == parseFloat(s3.value)) {
            c.value = "Isosceles Triangle";
        }
        else { //if given conditions becomes false,it becomes condition forscalene triangle
            c.value = "Scalene Triangle";
        }
        //pythagoras theorem
        if (Math.pow(parseFloat(s1.value), 2) + Math.pow(parseFloat(s2.value), 2) == Math.pow(parseFloat(s3.value), 2)) {
            r.value = "Right Angle Triangle";
        }
        else if (Math.pow(parseFloat(s2.value), 2) + Math.pow(parseFloat(s3.value), 2) == Math.pow(parseFloat(s1.value), 2)) {
            r.value = "Right Angle Triangle";
        }
        else if (Math.pow(parseFloat(s1.value), 2) + Math.pow(parseFloat(s3.value), 2) == Math.pow(parseFloat(s2.value), 2)) {
            r.value = "Right Angle Triangle";
        }
        else {
            r.value = "Not right angle triangle";
        }
    }
}
//# sourceMappingURL=triange.js.map