function for_loop() {
    var count;
    for (count = 1; count <= 100; count++) {
        console.log(count);
    }
}
function while_loop() {
    var count;
    count = 200;
    while (count > 100) {
        console.log(count);
        count--;
    }
}
//# sourceMappingURL=work5.js.map